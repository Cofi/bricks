# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# g7 <me@medesimo.eu>, 2013,2015
msgid ""
msgstr ""
"Project-Id-Version: bricks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-04 20:33+0200\n"
"PO-Revision-Date: 2015-04-04 18:35+0000\n"
"Last-Translator: g7 <me@medesimo.eu>\n"
"Language-Team: Italian (http://www.transifex.com/projects/p/bricks/language/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: libbricks/features.py:29
msgid "Bluetooth support"
msgstr "Supporto bluetooth"

#: libbricks/features.py:30
msgid "Without bluetooth support you can't use bluetooth devices."
msgstr "Senza il supporto bluetooth non è possibile usare i dispositivi bluetooth."

#: libbricks/features.py:38
msgid "Printing support"
msgstr "Supporto stampanti"

#: libbricks/features.py:39
msgid "This includes printer drivers and manage tools."
msgstr "Questo include i driver delle stampanti ed i tool di gestione."

#: libbricks/features.py:47
msgid "Office applications"
msgstr "Applicazioni per l'ufficio"

#: libbricks/features.py:48
msgid "Word processors and spreadsheets."
msgstr "Elaboratori di testo e fogli di calcolo."

#: libbricks/features.py:54
msgid "Visual effects"
msgstr "Effetti visivi"

#: libbricks/features.py:55
msgid "Visual effects such as transparencies, shadows, etc."
msgstr "Effetti visivi come trasparenze, ombre, ecc."

#: libbricks/features.py:61
msgid "Advanced audio support"
msgstr "Supporto avanzato per l'audio"

#: libbricks/features.py:62
msgid "Advanced audio support (PulseAudio)."
msgstr "Supporto avanzato per l'audio (PulseAudio)."

#: libbricks/features.py:69
msgid "Web applications support"
msgstr "Supporto applicazioni web"

#: libbricks/features.py:70
msgid "Support for web applications such as YouTube, Twitter and Facebook."
msgstr "Supporto per le applicazioni web come YouTube, Twitter e Facebook."

#: libbricks/features.py:76
msgid "Proprietary packages"
msgstr "Pacchetti proprietari"

#: libbricks/features.py:77
msgid ""
"Proprietary packages such as firmwares, the Adobe(R) Flash Player plugin and"
" more."
msgstr "Pacchetti proprietari come firmware, il plugin Adobe(R) Flash Player e altro."

#: bricks.py:49
msgid "Core packages"
msgstr "Pacchetti principali"

#: bricks.py:50
msgid "Graphical support tools"
msgstr "Pacchetti di supporto grafico"

#: bricks.py:213
msgid "Press Close to exit from the application."
msgstr "Premere Chiudi per uscire dall'applicazione."

#: bricks.py:532
msgid "Press the <i>Close</i> button to apply the changes."
msgstr "Premere il pulsante <i>Chiudi</i> per applicare i cambiamenti."

#: bricks.py:591
#, python-format
msgid "<b>%s</b> requires \"<i>%s</i>\", which is not currently enabled."
msgstr "<b>%s</b> richiede \"<i>%s</i>\", che è disabilitato."

#: bricks.py:610
msgid "USAGE: bricks <ERROR_APP> <ERROR_FEATURE>"
msgstr "USO: bricks <ERROR_APP> <ERROR_FEATURE>"

#: bricks.py:611
msgid "This application doesn't require any argument."
msgstr "Quest'applicazione non richiede alcun argomento."

#: bricks.py:612
msgid "ERROR_APP and ERROR_FEATURE are internally used."
msgstr "ERROR_APP e ERROR_FEATURE sono usati internamente."

#: bricks.py:617
msgid "ERROR: ERROR_APP requires also an ERROR_FEATURE."
msgstr "ERRORE: ERROR_APP richiede anche ERROR_FEATURE."

#: bricks.glade.h:1
msgid "<big><b>An error occurred.</b></big>"
msgstr "<big><b>È stato riscontrato un errore.</b></big>"

#: bricks.glade.h:2
msgid "Manage Semplice features"
msgstr "Gestisci le funzionalità di Semplice"

#: bricks.glade.h:3
msgid "label"
msgstr "stringa"

#: bricks.glade.h:4
msgid "Enable"
msgstr "Abilita"

#: bricks.glade.h:5
msgid "<b>Features</b>"
msgstr "<b>Funzionalità</b>"

#: bricks.glade.h:6
msgid "<i>Please wait...</i>"
msgstr "<i>Attendere prego...</i>"

#: bricks.glade.h:7
msgid "Customize features"
msgstr "Personalizza le funzionalità"
